import React from 'react';
import { ActionSheetIOS, ActivityIndicator, SafeAreaView, StatusBar, View, Text, StyleSheet } from 'react-native';
import { retrieveSavedFlights } from '../actions/index';
import CardStack from 'react-native-card-stack-swiper';
import { FlightCard } from '../components/FlightCard';
import { removeFlight } from '../actions/index';
import Toast, { DURATION } from 'react-native-easy-toast';

export default class ShortlistScreen extends React.Component {
  static navigationOptions = {
    title: 'Shortlist',
    headerStyle: {
      backgroundColor: '#6A3460'
    },
    headerTitleStyle: {
      color: '#fff',
      fontWeight: 'normal'
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      dimensions: undefined,
      savedFlights: [],
      isLoading: true,
      currency: 'EUR'
    }

    this.props.navigation.addListener('didFocus', () => {
      retrieveSavedFlights().then((data) => {
        console.log('RETRIVED DATA in HOME SCREEN');
        console.log(data);
        this.setState({savedFlights: data, isLoading: false});
      }).catch((err) => {
        throw err;
      });
    });
  }

  componentDidMount() {
    // retrieveSavedFlights().then((data) => {
    //   console.log('RETRIVED DATA in HOME SCREEN');
    //   console.log(data);
    //   this.setState({savedFlights: data, isLoading: false});
    // }).catch((err) => {
    //   throw err;
    // });
  }

  render() {
    console.log('RENDER');
    if (this.state.isLoading){
      return(
        <View style={{flex: 1}} onLayout={this.onLayout}>
          <View style={{flex: 1, padding: 20}}>
            <SafeAreaView>
              <StatusBar
                barStyle="light-content"
              />
            </SafeAreaView>
            <ActivityIndicator/>
          </View>
        </View>
      )
    }
    console.log(this.state.savedFlights.length);
    if (this.state.savedFlights.length > 0) {
      return (
        <View style={[{flex:1}, styles.parentContainer]} onLayout={this.onLayout}>
          <CardStack
            style={styles.cardStack}
            renderNoMoreCards={() => <Text style={{fontWeight: '700', fontSize: 18, color: 'gray'}}>No more cards :(</Text>}
            ref={swiper => {
              this.swiper = swiper
            }}
            onSwipedRight={() => console.log('onSwipedRight Stack')}
            onSwipedLeft={() => console.log('onSwipedLeft Stack')}
            >
              {this.state.savedFlights.map(function(flight, index) {
                console.log(flight.dimensions);
                  let data = {dimensions: flight.dimensions, flight, currency: flight.currency, index};
                      return <FlightCard 
                                  key={index} 
                                  data={data} 
                                  onSwipedRight={() => this.displayActionSheet()} 
                                  onSwipedLeft={() => removeFlight(flight.key).then(() => {
                                    console.log('DELETED');
                                  })}
                                  onSwipedTop={() => console.log('onSwipedTop')}
                                  onSwipedBottom={() => console.log('onSwipedBottom')}
                              />
                  }.bind(this))
              }
          </CardStack>
        </View>
      );
    } else {
      return (
        <View style={styles.noDealsView}><Text style={styles.noDealsText}>Swipe to save some amazing deals! :)</Text></View>
      );
    }
  }

  onLayout = event => {
    console.log('ON LAYOYT');
    if (this.state.dimensions) return // layout was already called
    let { width, height } = event.nativeEvent.layout
    this.setState({ dimensions: { width, height } })
  }

  displayActionSheet = () => {
    ActionSheetIOS.showActionSheetWithOptions({
      options: ['Cancel', 'Book', 'MoreInfo'],
      cancelButtonIndex: 0
    },
    (buttonIndex) => {
      if (buttonIndex === 1) { /* destructive action */ }
    });
  }

  deleteFlight = () => {

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  noDealsText: {
    fontWeight: '700', 
    fontSize: 18, 
    color: 'gray',
  },
  noDealsView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  parentContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  cardStack:{
    top: 5,
    flex: 4,
    alignItems: 'center',
    justifyContent: 'center'
  },
});
