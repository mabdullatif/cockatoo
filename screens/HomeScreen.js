import React from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  ActivityIndicator,
  Text,
  AsyncStorage,
  SafeAreaView,
  StatusBar
} from 'react-native';
import { WebBrowser } from 'expo';
import { fetchFlights } from '../actions/index';
import CardStack from 'react-native-card-stack-swiper';
import { FlightCard } from '../components/FlightCard';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'COCKATOO',
    headerStyle: {
      backgroundColor: '#6A3460'
    },
    headerTitleStyle: {
      color: '#fff',
      fontWeight: 'normal'
    }
  };

  constructor(props) {
    super(props)
    this.state = {
      dimensions: undefined,
      isLoading: true,
      flights: [],
      currency: 'EUR',
      swiper: undefined
    }
  }
  
  componentDidMount() {
    fetchFlights('BER', '2018-11-12', '2018-11-20', 3000, 2).then((data) => {
      this.setState ({isLoading: false, flights: data.flights, currency: data.currency});
    }).catch((err) => {
      throw err;
    });
  }

  render() {
    if (this.state.isLoading){
      return(
        <View style={{flex: 1}} onLayout={this.onLayout}>
          <View style={{flex: 1, padding: 20}}>
            <SafeAreaView>
              <StatusBar
                barStyle="light-content"
              />
            </SafeAreaView>
            <ActivityIndicator/>
          </View>
        </View>
      )
    }

    if (this.state.flights.length > 0) {
      return (
        <View style={{flex: 1}} onLayout={this.onLayout}>  
            <SafeAreaView>
              <StatusBar
                barStyle="light-content"
              />
            </SafeAreaView>        
          <View style={styles.parentContainer}>
            <CardStack
                style={styles.cardStack}
                renderNoMoreCards={() => <Text style={{fontWeight: '700', fontSize: 18, color: 'gray'}}>No more cards :(</Text>}
                ref={swiper => {
                  this.swiper = swiper
                }}
                onSwipedRight={() => console.log('onSwipedRight Stack')}
                onSwipedLeft={() => console.log('onSwipedLeft Stack')}
                >
                  {this.state.flights.map(function(flight, index) {
                      let data = {dimensions: this.state.dimensions, flight, currency: this.state.currency, index};
                          return <FlightCard 
                                      key={index} 
                                      data={data} 
                                      onSwipedRight={() => this._saveFlight(`Flight-${index}`, data.flight)} 
                                      onSwipedLeft={() => console.log('onSwipedLeft')}
                                      onSwipedTop={() => console.log('onSwipedTop')}
                                      onSwipedBottom={() => console.log('onSwipedBottom')}
                                  />
                      }.bind(this))
                  }
              </CardStack>
        
            <View style={styles.footer}>
              <View style={styles.buttonContainer}>
                <TouchableOpacity style={[styles.button,styles.red]} onPress={()=>{
                  this.swiper.swipeLeft();
                }}>
                  <Image source={require('../assets/images/red.png')} resizeMode={'contain'} style={{ height: 62, width: 62 }} />
                </TouchableOpacity>
                <TouchableOpacity style={[styles.button,styles.orange]} onPress={() => {
                  this.swiper.goBackFromLeft();
                }}>
                  <Image source={require('../assets/images/back.png')} resizeMode={'contain'} style={{ height: 32, width: 32, borderRadius: 5 }} />
                </TouchableOpacity>
                <TouchableOpacity style={[styles.button,styles.green]} onPress={()=>{
                  this.swiper.swipeRight();
                }}>
                  <Image source={require('../assets/images/green.png')} resizeMode={'contain'} style={{ height: 62, width: 62 }} />
                </TouchableOpacity>
              </View>
    
            </View>
          </View>
        </View>
      ); 
    }
  }

  onLayout = event => {
    if (this.state.dimensions) return // layout was already called
    let { width, height } = event.nativeEvent.layout
    this.setState({ dimensions: { width, height } })
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };

  _saveFlight = async (key, flight) => {
      try {
        flight.currency = this.state.currency;
        flight.key = key;
        flight.dimensions = this.state.dimensions;
        var jsonOfItem = await AsyncStorage.setItem(key, JSON.stringify(flight));
        return jsonOfItem;
      } catch (error) {
        console.log(error);
      }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f2f2f2',
    paddingTop: 30
  },
  parentContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  cardStack:{
    top: 5,
    flex: 4,
    alignItems: 'center',
    justifyContent: 'center'
  },
  card: {
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#d6d7da',
    position: 'relative'
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  footer: {
    flex:1,
    justifyContent:'center',
    alignItems:'center',
  },
  buttonContainer:{
    width: 220,
    flexDirection:'row',
    justifyContent: 'space-between',
  },
  button:{
    shadowColor: 'rgba(0,0,0,0.3)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity:0.5,
    backgroundColor:'#fff',
    alignItems:'center',
    justifyContent:'center',
    zIndex: 0,
  },
  orange:{
    width:55,
    height:55,
    borderWidth:6,
    borderColor:'rgb(246,190,66)',
    borderWidth:4,
    borderRadius:55,
    marginTop:-15
  },
  green:{
    width:75,
    height:75,
    backgroundColor:'#fff',
    borderRadius:75,
    borderWidth:6,
    borderColor:'#01df8a',
  },
  red:{
    width:75,
    height:75,
    backgroundColor:'#fff',
    borderRadius:75,
    borderWidth:6,
    borderColor:'#fd267d',
  }
});
