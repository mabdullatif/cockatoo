import React from 'react';
import { Card } from 'react-native-card-stack-swiper';
import { Text, Image, StyleSheet, View } from 'react-native';
import getSymbolFromCurrency from 'currency-symbol-map';

export class FlightCard extends React.Component {
    render() {
        return (
            <Card style={[styles.card, {width: this.props.data.dimensions.width * 0.9, height: this.props.data.dimensions.height * 0.67}]}>
                <View style={styles.cardImage}>
                    <Image
                        source={{uri: this.props.data.flight.info.destinationimage}}
                        style={{backgroundColor: '#ECECEC', width: this.props.data.dimensions.width * 0.9, height: this.props.data.dimensions.height * 0.5, resizeMode: 'cover'}}
                    />
                    <View style={styles.cardTitleContainer}>
                        <View style={styles.cardHorizontalTitleContainer}>
                            <Text style={styles.cardTitle}>{this.props.data.flight.portions[0].dest.label}</Text>
                            <Text style={styles.cardPrice}>{getSymbolFromCurrency(this.props.data.currency)}{this.props.data.flight.price}</Text>
                        </View>
                        <View style={styles.cardHorizontalOutboundDatesContainer}>
                            <Text style={styles.cardDateRange}>Outbound: {this.props.data.flight.portions[0].dep.dateformatted}</Text>
                            <Text style={styles.cardTime}>{this.props.data.flight.portions[0].dep.timeformatted}</Text>
                        </View>
                        <View style={styles.cardHorizontalReturnDatesContainer}>
                            <Text style={styles.cardDateRange}>Return: {this.props.data.flight.portions[1].dep.dateformatted}</Text>
                            <Text style={styles.cardTime}>{this.props.data.flight.portions[1].dep.timeformatted}</Text>
                        </View>
                    </View>
                </View>
            </Card>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#d6d7da',
        position: 'relative',
        backgroundColor: '#fff'
    },
    cardImage: {
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        overflow: 'hidden',
        marginBottom: 10
    },
    cardTitleContainer: {
        marginTop: 8,
        marginLeft: 12,
        marginRight: 12,
        marginBottom: 8,
        flexDirection: 'column'
    },
    cardHorizontalTitleContainer: {
        marginTop: 2,
        marginBottom: 2,
        marginLeft: 4,
        marginRight: 4,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    cardHorizontalOutboundDatesContainer: {
        marginTop: 5,
        marginBottom: 2,
        marginLeft: 4,
        marginRight: 4,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    cardHorizontalReturnDatesContainer: {
        marginTop: 2,
        marginBottom: 2,
        marginLeft: 4,
        marginRight: 4,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    cardTitle: {
        fontSize: 22,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        fontWeight: 'bold',
        color: '#333'
    },
    cardPrice: {
        fontSize: 22,
        justifyContent: 'flex-end',
        flexDirection: 'row',
        fontWeight: 'bold',
        color: '#333'
    },
    cardDateRange: {
        marginTop: 2,
        fontSize: 12,
        flexDirection: 'row',
        color: '#888'
    },
    cardTime: {
        marginTop: 2,
        fontSize: 12,
        flexDirection: 'row',
        color: '#888'
    }
});