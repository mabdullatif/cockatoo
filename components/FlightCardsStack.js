import React from 'react';
import CardStack from 'react-native-card-stack-swiper';
import { StyleSheet, Text } from 'react-native';
import { FlightCard } from './FlightCard';

export class FlightCardsStack extends React.Component {
    render() {
        return (
            <CardStack
              style={styles.cardStack}
              renderNoMoreCards={() => <Text style={{fontWeight: '700', fontSize: 18, color: 'gray'}}>No more cards :(</Text>}
              ref={swiper => {
                this.swiper = swiper
              }}
              onSwipedRight={() => console.log('onSwipedRight')}
              onSwipedLeft={() => console.log('onSwipedLeft')}
              >
                {this.props.flights.map(function(flight, index) {
                    let data = {dimensions: this.props.dimensions, flight};
                        return <FlightCard 
                                    key={index} 
                                    data={data} 
                                    onSwipedRight={() => console.log('onSwipedRight')} 
                                    onSwipedLeft={() => console.log('onSwipedLeft')}
                                    onSwipedTop={() => console.log('onSwipedTop')}
                                    onSwipedBottom={() => console.log('onSwipedBottom')}
                                />
                    }.bind(this))
                }
            </CardStack>
        );
    }
}

const styles = StyleSheet.create({
    cardStack:{
        top: 30,
        flex: 5,
        alignItems: 'center',
        justifyContent: 'center'
      }
});
