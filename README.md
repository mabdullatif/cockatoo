Cockatoo
===

For people who want to go on a short trip in Europe, Cockatoo is an easy to use inspirational tool that suggests flight deals for random destinations based on users preferences. Its binary choices help the users to narrow down the results.

## What is a Cockatoo?

A cockatoo is a parrot that is any of the 21 species belonging to the bird family Cacatuidae, the only family in the superfamily Cacatuoidea. Along with the Psittacoidea (true parrots) and the Strigopoidea (large New Zealand parrots), they make up the order Psittaciformes (parrots). The family has a mainly Australasian distribution, ranging from the Philippines and the eastern Indonesian islands of Wallacea to New Guinea, the Solomon Islands and Australia.

We chose 'Cockatoo' as a name for our project because it's a symbol for parrots
who love to travel.

## Setup

This project was bootstrapped with [Expo](https://expo.io/).

## How to run

Clone the project directory, then in the root of your directory run `npm install`.
After that, run `expo start` and this will start a development server for you.

## Screenshots
![Cockatoo Screenshot](https://i.imgur.com/E4kCiaZ.png)
![Cockatoo Screenshot](https://i.imgur.com/rKlx7pd.png)
![Cockatoo Screenshot](https://i.imgur.com/hyJdhkh.png)
