import { AsyncStorage } from 'react-native';

const ROOT_URL = 'https://gw01.holidaypirates.com:5224/cnfc.php';

export async function fetchFlights(origin, from, to, budget, number_of_people) {
    const url = `${ROOT_URL}?orig=${origin}&depart=${from}&return=${to}&budget=${budget}&weekends=false&adults=${number_of_people}&country=uk`;
    
     try {
        let response = await fetch(url);
        let responseJson = await response.json();
        return {flights: responseJson.data.items, currency: responseJson.config.cur};
    } catch (error) {
        console.error(error);
    }
}

export async function retrieveSavedFlights () {
    try {
      return new Promise((resolve, reject) => {
        AsyncStorage.getAllKeys(async (err, keys) => {
            let flights = []
            await asyncForEach(keys, async (key) => {
              try {
                const retrievedItem = await AsyncStorage.getItem(key);
                console.log('RETRIEVED ITEM');
                console.log(retrievedItem);
                if (retrievedItem) {
                    flights.push(JSON.parse(retrievedItem));
                }
              } catch (error) {
                console.log(error.message);
              }
            });
            console.log('RETURNING SAVED FLIGHTS');
            console.log(flights);
            resolve(flights);
          });
      });
    } catch (error) {
      console.log(error.message);
    }
}

export async function removeFlight(key) {
    console.log(`DELETING ${key}`);
    try {
      await AsyncStorage.removeItem(key);
      return true;
    }
    catch(exception) {
      return false;
    }
  }

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array)
    }
  }